<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <managedEntities>
        <managedEntityGroup name="DEV">
            <attribute name="ENV">DEV</attribute>
            <addTypes>
                <type ref="System Checks">
                    <environment ref="EMEADEV"></environment>
                </type>
            </addTypes>
            <managedEntityGroup name="AppServer">
                <attribute name="ENTITY">Application Server</attribute>
                <addTypes>
                    <type ref="App Servers">
                        <environment ref="EMEADEV"></environment>
                    </type>
                </addTypes>
                <managedEntity name="Application Developer Server">
                    <probe ref="AppDev"></probe>
                    <addTypes>
                        <type ref="App Dev">
                            <environment ref="EMEADEV"></environment>
                        </type>
                    </addTypes>
                </managedEntity>
            </managedEntityGroup>
        </managedEntityGroup>
        <managedEntityGroup name="TEST">
            <attribute name="ENV">TEST</attribute>
            <addTypes>
                <type ref="System Checks">
                    <environment ref="EMEATEST"></environment>
                </type>
            </addTypes>
            <managedEntityGroup name="AppServer">
                <attribute name="ENTITY">Application Server</attribute>
                <addTypes>
                    <type ref="App Servers">
                        <environment ref="EMEATEST"></environment>
                    </type>
                </addTypes>
                <managedEntity name="Application UAT Server">
                    <probe ref="AppUAT"></probe>
                    <addTypes>
                        <type ref="UAT">
                            <environment ref="EMEATEST"></environment>
                        </type>
                    </addTypes>
                </managedEntity>
            </managedEntityGroup>
        </managedEntityGroup>
        <managedEntityGroup name="PROD">
            <attribute name="ENV">PROD</attribute>
            <addTypes>
                <type ref="System Checks">
                    <environment ref="EMEAPROD"></environment>
                </type>
            </addTypes>
            <managedEntity name="ELK Server">
                <probe ref="ELK"></probe>
                <addTypes>
                    <type ref="ELK">
                        <environment ref="EMEAPROD"></environment>
                    </type>
                </addTypes>
            </managedEntity>
            <managedEntityGroup name="AppServer">
                <attribute name="ENTITY">Application Server</attribute>
                <addTypes>
                    <type ref="App Servers">
                        <environment ref="EMEAPROD"></environment>
                    </type>
                </addTypes>
                <managedEntity name="Application Production Server">
                    <probe ref="AppProd"></probe>
                    <addTypes>
                        <type ref="Prod">
                            <environment ref="EMEAPROD"></environment>
                        </type>
                    </addTypes>
                </managedEntity>
            </managedEntityGroup>
            <managedEntityGroup name="Deployment">
                <managedEntity name="Deployment Server">
                    <probe ref="uDeploy"></probe>
                    <addTypes>
                        <type ref="Deployment">
                            <environment ref="EMEAPROD"></environment>
                        </type>
                    </addTypes>
                </managedEntity>
            </managedEntityGroup>
            <managedEntityGroup name="Docker Registry">
                <managedEntity name="Docker Registry">
                    <probe ref="Docker Registry"></probe>
                    <addTypes>
                        <type ref="Docker Reg">
                            <environment ref="EMEAPROD"></environment>
                        </type>
                    </addTypes>
                </managedEntity>
            </managedEntityGroup>
        </managedEntityGroup>
    </managedEntities>
    <samplers></samplers>
    <environments>
        <environmentGroup name="EMEA">
            <environment name="EMEADEV">
                <var name="IntervalTime">
                    <integer>10</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
            <environment name="EMEAPROD">
                <var name="IntervalTime">
                    <integer>30</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
            <environment name="EMEATEST">
                <var name="IntervalTime">
                    <integer>30</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
        </environmentGroup>
    </environments>
</gateway>