<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <actions>
        <action name="Action Docker None Check">
            <command ref="Command Docker None Check">
                <args></args>
            </command>
        </action>
        <action name="Restart Kill Zombie P">
            <command ref="Restart Kill Zombie Processes">
                <args></args>
            </command>
        </action>
        <action name="Restart Open Shift">
            <command ref="Restart Open Shift">
                <args></args>
            </command>
        </action>
        <action name="Trade App - Delete Completed and Error Pods">
            <command ref="Trade App - Delete Completed and Error Pods">
                <args></args>
            </command>
        </action>
        <action name="nycautotrader - Delete Completed and Error Pods">
            <command ref="nycautotrader - Delete Completed and Error Pods">
                <args></args>
            </command>
        </action>
        <action name="Delete Persistent Volumes">
            <command ref="Delete Persistent Volumes">
                <args></args>
            </command>
        </action>
    </actions>
    <commands>
        <command name="Command Docker None Check">
            <userCommand>
                <type>script</type>
                <runLocation>gateway</runLocation>
                <args>
                    <arg>
                        <static>/usr/bin/docker rmi `/usr/bin/docker images -f &quot;dangling=true&quot; -q`</static>
                    </arg>
                </args>
            </userCommand>
        </command>
        <command name="Restart Kill Zombie Processes">
            <userCommand>
                <type>script</type>
                <runLocation>netprobe</runLocation>
                <args>
                    <arg>
                        <static>init 6</static>
                    </arg>
                </args>
            </userCommand>
        </command>
        <command name="Restart Open Shift">
            <userCommand>
                <type>script</type>
                <runLocation>netprobe</runLocation>
                <args>
                    <arg>
                        <static>service openshift start</static>
                    </arg>
                </args>
            </userCommand>
        </command>
        <command name="Trade App - Delete Completed and Error Pods">
            <userCommand>
                <type>script</type>
                <runLocation>netprobe</runLocation>
                <args>
                    <arg>
                        <static>/bin/bash -c  &quot;source /root/.bashrc; /bin/oc project trades &gt;/dev/null 2&gt;/dev/null; /usr/bin/oc delete pods `/usr/bin/oc get pods | /usr/bin/egrep &quot;Completed|Error&quot; | /usr/bin/egrep 0/1 | /usr/bin/awk &apos;{print $1}&apos;`&quot;</static>
                    </arg>
                </args>
            </userCommand>
        </command>
        <command name="nycautotrader - Delete Completed and Error Pods">
            <userCommand>
                <type>script</type>
                <runLocation>netprobe</runLocation>
                <args>
                    <arg>
                        <static>/bin/bash -c  &apos;source /root/.bashrc; /usr/bin/oc project nycautotrader &gt;/dev/null 2&gt;/dev/null; for pod in `/usr/bin/oc get pods | /usr/bin/egrep &quot;Completed|Error&quot; | /usr/bin/egrep &quot;0/1&quot; | /usr/bin/cut -d&quot; &quot; -f1`; do /usr/bin/oc delete pods $pod 2&gt;&amp;1; done&apos;</static>
                    </arg>
                </args>
            </userCommand>
        </command>
        <command name="Delete Persistent Volumes">
            <userCommand>
                <type>script</type>
                <runLocation>gateway</runLocation>
                <args>
                    <arg>
                        <static>/bin/bash -c  &apos;source /root/.bashrc; for vols in `oc get pv | grep Available | cut -d &quot; &quot; -f1`;do oc delete pv $vols 2&gt;&amp;1;done&apos;</static>
                    </arg>
                </args>
            </userCommand>
        </command>
    </commands>
</gateway>